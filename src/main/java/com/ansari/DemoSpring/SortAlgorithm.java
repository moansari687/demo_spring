package com.ansari.DemoSpring;

public interface SortAlgorithm {
    public int[] sort(int[] numbers);
}

package com.ansari.DemoSpring;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
//@Primary
public class QuickSortAlgorithm implements SortAlgorithm{
    public int[] sort(int[] numbers){
        //logic
        Arrays.sort(numbers);
        return numbers;
    }
}

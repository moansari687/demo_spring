package com.ansari.DemoSpring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
//for Component Scanning
@ComponentScan(basePackages = "com.ansari")
public class AppConfig {


    @Bean(name = "quickSort")
    public SortAlgorithm quickSortAlgorithm() {
        return new QuickSortAlgorithm();
    }

    @Bean(name = "bubbleSort")
    public SortAlgorithm bubbleSortAlgorithm() {
        return new BubbleSortAlgorithm();
    }


    @Bean
    public SearchAlgorithm linearSearch() {
        return new LinearSearchImpl();
    }

    @Bean
    public SearchAlgorithm binarySearch() {
        return new BinarySearchImpl();
    }
}

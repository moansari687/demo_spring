package com.ansari.DemoSpring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

//@Component
public class BinarySearchImpl implements SearchAlgorithm{

    //@Autowired
    @Resource
    SortAlgorithm quickSortAlgorithm;

//    public BinarySearchImpl() {
//
//    }

    public boolean search(int[] numbers, int numberToSearch){
        //logic
        //sort
        //BubbleSortAlgorithm bubbleSortAlgorithm = new BubbleSortAlgorithm();
        int[] sortedNumbers = quickSortAlgorithm.sort(numbers);
        System.out.println(quickSortAlgorithm);
        //
        return true;
    }
}

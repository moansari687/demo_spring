package com.ansari.DemoSpring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

//@Component
public class LinearSearchImpl implements SearchAlgorithm{

    //@Autowired
    @Resource(name = "quickSort")
    SortAlgorithm sortAlgorithm;

    public boolean search(int[] numbers, int numberToSearch){

        int[] sortedNumbers = sortAlgorithm.sort(numbers);
        System.out.println(sortAlgorithm);
        for(int i : sortedNumbers){
            if(numberToSearch == i)
                return true;
        }
        return false;
    }
}

package com.ansari.DemoSpring;

import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public class BubbleSortAlgorithm implements SortAlgorithm{
    public int[] sort(int[] numbers){
        //logic
        Arrays.sort(numbers);
        return numbers;
    }
}

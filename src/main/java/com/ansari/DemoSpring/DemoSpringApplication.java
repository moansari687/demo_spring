package com.ansari.DemoSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class DemoSpringApplication {

	public static void main(String[] args) {

		//With normal java
		//BinarySearchImpl binarySearch = new BinarySearchImpl(new QuickSortAlgorithm());
		//System.out.println(result);


		//"lite" mode with Component Annotations
		//ApplicationContext applicationContext =
		//		SpringApplication.run(DemoSpringApplication.class, args);
		//BinarySearchImpl binarySearch =
		//		applicationContext.getBean(BinarySearchImpl.class);

		//int result = binarySearch.binarySearch(new int[] {12,4,6},3);
		//System.out.println(result);


		//with java config class
		ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
		//or
//		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
//		ctx.register(AppConfig.class);
//		ctx.scan("com.ansari");
//		ctx.refresh();

		SearchAlgorithm linearSearch = ctx.getBean(LinearSearchImpl.class);

		boolean result = linearSearch.search(new int[] {12,3,6},6);
		if(result)
			System.out.println("Found");
		else
			System.out.println("Not Found");

	}

}

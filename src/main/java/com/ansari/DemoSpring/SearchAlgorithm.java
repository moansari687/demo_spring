package com.ansari.DemoSpring;

public interface SearchAlgorithm {
    public boolean search(int[] numbers,int key);
}
